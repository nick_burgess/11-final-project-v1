package pkgfinal.project.v1;

public class Input {

    private static Input instance;

    public static Input getInstance() {
        if (instance == null) {
            instance = new Input(); 
        }
        return instance;
    }

    public boolean getJump() {
        return Screen.canvas.isKeyPressed('w');
    }

    public boolean getLeft() {
        return Screen.canvas.isKeyPressed('a');
    }

    public boolean getRight() {
        return Screen.canvas.isKeyPressed('d');
    }
}
