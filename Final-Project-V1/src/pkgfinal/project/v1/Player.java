package pkgfinal.project.v1;

import DLibX.DCanvas;
import DLibX.DConsole;
import java.awt.Color;

public class Player {

    private Input in = Input.getInstance();
    
    private int size = 30;
    private Color color = Color.BLACK;
    private int xPos;
    private int yPos;
    private int xChange = 0;
    private int yChange = 0;
    private int groundHeight = 585;
    private double gravity = 1.05;
    

    public Player(int x, int y, int size) {
        this.xPos = x;
        this.yPos = y;
        this.size = size;
    }

    public void setX(int x) {
        this.xPos = x;
    }

    public int getX() {
        return this.xPos;
    }

    public void setY(int y) {
        this.yPos = y;
    }

    public int getY() {
        return this.yPos;
    }

    public void setColor(Color c) {
        this.color = c;
    }

    public Color getColor() {
        return this.color;
    }

    public void setXChange(int xChange) {
        this.xChange = xChange;
    }

    public double getXChange() {
        return this.xChange;
    }

    public void setYChange(int yChange) {
        this.yChange = yChange;
    }

    public double getYChange() {
        return this.yChange;
    }

    public void move() {
        this.setX(this.getX() + this.xChange);
        this.setY(this.getY() + this.yChange);
    }
    
    public void jump() {
        if(in.getJump()) {
            
        }
    }

    public void update() {
        Screen.canvas.setPaint(this.color);
        Screen.canvas.setOrigin(DConsole.ORIGIN_CENTER);
        Screen.canvas.fillEllipse(this.xPos, this.yPos, this.size, this.size);
    }

}
