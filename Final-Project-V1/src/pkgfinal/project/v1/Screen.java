package pkgfinal.project.v1;

import DLibX.DConsole;
import DLibX.util.Fps;
import java.text.DecimalFormat;

public class Screen {

    private static final double smoothing = 0.85;
    private static final Fps f = new Fps(Screen.smoothing);
    private static DecimalFormat df = new DecimalFormat("#.#");

    public static double getFPS() {
        f.update();
        return Double.valueOf(df.format(f.getFps()));
    }

    private static final String name = "Insert Name Here";

    public static DConsole canvas = new DConsole(name, 600, 600, true);
    public static DConsole data = new DConsole("Game Data", 350, 600, true);

}
