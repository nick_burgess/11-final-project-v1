package pkgfinal.project.v1;


public class Debugger {

    private static Debugger instance;

    public static Debugger getInstance() {
        if (instance == null) {
            instance = new Debugger();
        }
        return instance;
    }

    public void add(String name, int i, int y) {
        Screen.data.drawString(name + ": " + i, 5, y);
    }

    public void add(String name, boolean b, int y) {
        Screen.data.drawString(name + ": " + b, 5, y);
    }

    public void add(String name, double d, int y) {
        Screen.data.drawString(name + ": " + d, 5, y);
    }

    public void add(String name, String s, int y) {
        Screen.data.drawString(name + ": " + s, 5, y);
    }

    public void add(String name, Byte b, int y) {
        Screen.data.drawString(name + ": " + b, 5, y);
    }

    public void add(String name, Short s, int y) {
        Screen.data.drawString(name + ": " + s, 5, y);
    }

    public void add(String name, char c, int y) {
        Screen.data.drawString(name + ": " + c, 5, y);
    }

    public void add(String name, float f, int y) {
        Screen.data.drawString(name + ": " + f, 5, y);
    }

}
