package pkgfinal.project.v1;

import DLibX.DConsole;
import java.awt.Font;


public class Main {

    public static void main(String[] args) {
        int mainX = 100;
        int mainY = 100;
        
        
        Debugger d = Debugger.getInstance();
        

        Screen.data.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
        while (true) {
            Screen.canvas.clear();
            Screen.data.clear();
            Screen.canvas.setOrigin(DConsole.ORIGIN_CENTER);

            mainX = Screen.canvas.getFrame().getLocation().x;
            mainY = Screen.canvas.getFrame().getLocation().y;
            
            d.add("FPS", Screen.getFPS(),20);
           

            Screen.data.getFrame().setLocation(mainX + Screen.canvas.getWidth(), mainY);

            Screen.canvas.redraw();
            Screen.data.redraw();
            Screen.data.pause(20);
        }
    }

}
